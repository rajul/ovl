#!/usr/bin/python
import re
import json

class OvlUserCSVRecordValidator:
	NUMBER_OF_FIELDS_IN_CSV = 5
	EXPECTED_FIELDS = ("email", "name", "phone", "organisation", "group")
	EMAIL_REGEX = re.compile(r'[^@]+@[^@]+\.[^@]+')
	NAME_REGEX = re.compile(r'[a-zA-Z\-\'\s]+')

	def __init__(self):
		self.csv_data = []
		self.data = {}

	def validate_record_number_of_fields(self):
		return len(self.csv_data) == self.NUMBER_OF_FIELDS_IN_CSV

	def create_data_map(self):
		for i in range(self.NUMBER_OF_FIELDS_IN_CSV):
			self.data[self.EXPECTED_FIELDS[i]] = self.csv_data[i] 

	def validate_required_fields(self):
		return True

	def validate_record_email_address(self):
		print self.EMAIL_REGEX.match(self.data['email'])
		if not self.EMAIL_REGEX.match(self.data['email']):
			return False

		return True

	def validate_real_name(self):
		if not self.NAME_REGEX.match(self.data['name']):
			return False

		return True

	def validate_phone_number(self):
		return True

	def validate_organisation(self):
		return True

	def validate_group(self):
		return True

	def validate_record(self, csv_line):
		self.csv_data = csv_line.strip().split(',')

		print self.csv_data
		print self.validate_record_number_of_fields()

		if self.validate_record_number_of_fields():
			self.create_data_map()
		else:
			return False

		if not self.validate_record_email_address():
			return False

		if not self.validate_real_name():
			return False

		if not self.validate_phone_number():
			return False

		if not self.validate_group():
			return False

		if not self.validate_organisation():
			return False

		return True


class OvlUserCSVFileValidator:		
	def __init__(self, filepath):
		self.file = open(filepath, 'r')

		self.good_records = []
		self.bad_records = []

		self.good_records_count = 0
		self.bad_records_count = 0

		self.record_validator = OvlUserCSVRecordValidator()

	def validate_file(self):
		for line in self.file:
			if self.record_validator.validate_record(line):
				self.good_records.append(line)
				self.good_records_count = self.good_records_count + 1
			else:
				self.bad_records.append(line)
				self.bad_records_count = self.bad_records_count + 1

		return True

