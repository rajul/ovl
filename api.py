from flask import Flask, render_template, request
from werkzeug import secure_filename

from ticket import OvlRTApprovalTicketCreator

app = Flask(__name__)
UPLOAD_FOLDER = 'upload'

@app.route('/')
def upload_file():
  return render_template('index.html')
	
@app.route('/upload', methods = ['POST'])
def upload_file_handler():
   if request.method == 'POST':
      f = request.files['file']
      f.save(UPLOAD_FOLDER + '/' + secure_filename(f.filename))

      ticket_creator = OvlRTApprovalTicketCreator(UPLOAD_FOLDER + '/' + secure_filename(f.filename))
      ticket_creator.create_tickets()

      return 'file uploaded successfully'
		
if __name__ == '__main__':
   app.run(debug = True)