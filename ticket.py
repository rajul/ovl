#!/usr/bin/python
import re

from rtkit.resource import RTResource
from rtkit.authenticators import BasicAuthenticator, CookieAuthenticator
from rtkit.errors import RTResourceError

from rtkit import set_logging
import logging

import requests,logging
import json

from validator import OvlUserCSVFileValidator

set_logging('debug')
logger = logging.getLogger('rtkit')

class OvlRTApprovalTicketCreator:
	RT_HOST = 'localhost:8080'
	USER = 'root'
	PASSWORD = 'password'

	# RT_HOST = 'https://dev.ourvotelive.org'
	# USER = 'root'
	# PASSWORD = 'password'

	APPROVALS_QUEUE = 'UserCreationApprovalTest'
	EXCEPTIONS_QUEUE = 'UserCreationExceptionTest'

	APPROVALS_SUBJECT = 'DummySubjectApproval'
	EXCEPTIONS_SUBJECT = 'DummySubjectApproval'

	APPROVALS_TEXT = 'DummyTextApproval'
	EXCEPTIONS_TEXT = 'DummyTextApproval'

	def __init__(self, uploaded_filepath):
		self.file_validator = OvlUserCSVFileValidator(uploaded_filepath)
		self.file_validator.validate_file()

		self.rt_url = self.get_rt_url_string()
		self.ticket_creation_url = self.get_rt_ticket_creation_url_string('ticket/new')

		self.rt_connection = RTResource(self.rt_url, self.USER, self.PASSWORD, CookieAuthenticator)

	def get_rt_url_string(self):
		return 'http://' + self.RT_HOST + '/REST/1.0/'


	def get_rt_ticket_creation_url_string(self, suffix):
		return self.rt_url + suffix + self.get_user_string()

	def get_user_string(self):
		return '?user=' + self.USER + '&pass=' + self.PASSWORD

	def comment_with_attachment(self, ticket_id, attachment_file):
		try:
		    params = {
		        'content': {
		            'Action': 'comment',
		            'Text': 'Comment with attach',
		            'Attachment': 'x.csv',
		        },
		        'attachment_1': file(attachment_file)
		    }

		    comment_path = 'ticket/' + str(ticket_id) + '/comment'

		    response = resource.post(path=comment_path, payload=params,)
		    for r in response.parsed:
		            logger.info(r)
		except RTResourceError as e:
		    logger.error(e.response.status_int)
		    logger.error(e.response.status)
		    logger.error(e.response.parsed)


	def create_good_record_ticket(self):
		content = {
			'content': {
				'Queue': self.APPROVALS_QUEUE,
				'Subject': self.APPROVALS_SUBJECT,
				'Text': '\n'.join(self.file_validator.good_records)
			}
		}

		# post_data = r'id: ticket/new\nQueue: %s\nSubject: %s\nText: %s\nRequestor: brian@dugga.net' %(self.APPROVALS_QUEUE, self.APPROVALS_SUBJECT, self.file_validator.good_records.join('\n'))

		# print post_data
		# print self.ticket_creation_url

		# payload = {'user': self.USER, 'pass': self.PASSWORD,'content':post_data}
		# print payload
		try:
			# response = requests.post(self.ticket_creation_url, payload)
			response = self.rt_connection.post(path='ticket/new', payload=content,)		
			logger.info(response.parsed)
			# logger.info(response)
		except Exception as e:
			# logger.error(e.text)
			print e
			logger.error(e.response.status_int)
			logger.error(e.response.status)
			logger.error(e.response.parsed)
			
		
	def create_bad_record_ticket(self):
		content = {
			'content': {
				'Queue': self.EXCEPTIONS_QUEUE,
				'Subject': self.EXCEPTIONS_SUBJECT,
				'Text': '\n'.join(self.file_validator.bad_records)
			}
		}

		try:
			response = self.rt_connection.post(path='ticket/new' + self.get_user_string(), payload=content,)		
			logger.info(response.parsed)
		except RTResourceError as e:
			print e
			logger.error(e.response.status_int)
			logger.error(e.response.status)
			logger.error(e.response.parsed)

	def create_tickets(self):
		self.create_good_record_ticket()
		self.create_bad_record_ticket()
